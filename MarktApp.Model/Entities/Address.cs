﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarktApp.Model.Entities
{
    public class Address
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Country field is required")]
        [Display(Name = "Country")]
        public string Country { get; set; }
        [Display(Name = "State")]
        public string State { get; set; }
        [Required(ErrorMessage = "City field is required")]
        [Display(Name = "City")]
        public string City { get; set; }
        [Required(ErrorMessage = "Street field is required")]
        [Display(Name = "Street name")]
        public string Street { get; set; }
        [Required(ErrorMessage ="Street number field is required")]
        [Display(Name = "Street number")]
        public int StreetNumber { get; set; }
        public int? HouseNumber { get; set; }
        [Required(ErrorMessage = "Zip code field is required")]
        [Display(Name = "Zip code")]
        public string ZipCode { get; set; }
        public virtual Customer Customer { get; set; }
    }
}