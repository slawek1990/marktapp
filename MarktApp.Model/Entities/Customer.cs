﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarktApp.Model.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "First name field is required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name field is required")]
        [Display(Name = "Last name" )]
        public string LastName { get; set; }
        [Display(Name = "Phone number")]
        public string PhoneNumber  { get; set; }
        public virtual Address Address { get; set; }
    }
}
