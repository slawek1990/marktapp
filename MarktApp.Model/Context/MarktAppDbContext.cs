﻿using MarktApp.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarktApp.Model.Context
{
    public class MarktAppDbContext : DbContext
    {
        public MarktAppDbContext() : base("MarktAppConnectionString") { }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasOptional(x => x.Address)
                .WithRequired(x => x.Customer).WillCascadeOnDelete(true) ;
        }


        public virtual DbSet<Customer> Customers { get; set; }

        public System.Data.Entity.DbSet<MarktApp.Model.Entities.Address> Addresses { get; set; }
    }
}
