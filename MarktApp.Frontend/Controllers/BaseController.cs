﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarktApp.Frontend.Controllers
{
    public class BaseController : Controller
    {
        public void AlertSuccess(string message)
        {
            TempData["success"] = message;
        }

        public void AlertWarning(string message)
        {
            TempData["warning"] = message;
        }

        public void AlertError(string message)
        {
            TempData["error"] = message;
        }
    }
}