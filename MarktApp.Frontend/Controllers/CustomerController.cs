﻿using MarktApp.Model.Entities;
using MarktApp.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarktApp.Frontend.Controllers
{
    public class CustomerController : BaseController
    {
        private IBaseRepository<Customer> _customerRepo;
        private IBaseRepository<Address> _addresRepo;
        public CustomerController()
        {
            _customerRepo = new BaseRepository<Customer>();
            _addresRepo = new BaseRepository<Address>();
        }
       
        public ActionResult Index()
        {
            var customers = _customerRepo.GetAll();

            return View(customers);
        }

        public ActionResult Details(int id)
        {
            var customer = _customerRepo.GetById(id);
            return View(customer);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var customer = _customerRepo.GetById(id);
            return View(customer);
        }

        [HttpPost]
        public ActionResult Edit(Customer customer)
        {
            try
            {
                _addresRepo.Update(customer.Address);
                _customerRepo.Update(customer);
                AlertSuccess("Customer has been updated");
            }
            catch (Exception ex)
            {
                AlertError("There was problem with update");
                //ToDo: LogException(ex)
            }
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            try
            {
                _customerRepo.Insert(customer);
                AlertSuccess("Customer has been added");
            }
            catch (Exception ex)
            {
                AlertError("There was problem with insert");
                //ToDo: LogException(ex)
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            try
            {
                _customerRepo.Delete(id);
                AlertSuccess("Customer has been deleted");
            }
            catch ( Exception ex)
            {
                AlertError("There was problem with delete");
                //ToDo: LogException(ex)
            }
            return RedirectToAction("Index");
        }
    }
}