﻿using MarktApp.Model.Context;
using MarktApp.Model.Entities;
using MarktApp.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarktApp.Frontend.Controllers
{
    public class HomeController : Controller
    {
        BaseRepository<Customer> repo = new BaseRepository<Customer>();

        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult About()
        {
            Address address = new Address()
            {
                Country = "USA",
                State = "Washington",
                City = "Seatle",
                Street = "Test",
                StreetNumber = 23,
                HouseNumber = 2,
                ZipCode = "23-32"
            };

            Customer customer = new Customer()
            {
                FirstName = "John",
                LastName = "Knoxwille",
                PhoneNumber = "232321332",
                Address = address
            };

            repo.Insert(customer);


            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}