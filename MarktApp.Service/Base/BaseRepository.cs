﻿using MarktApp.Model.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarktApp.Service.Base
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private MarktAppDbContext _context;
        private DbSet<T> _dbSet;

        public BaseRepository()
        {
            _context = new MarktAppDbContext();
            _dbSet = _context.Set<T>();
        }
        public void Delete(T entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            T entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public IQueryable<T> GetAll()
        {
            IQueryable<T> query = _dbSet;
            return query;
        }

        public T GetById(int id)
        {
            return _dbSet.Find(id);
        } 

        public void Insert(T entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
        }

        public void Update(T entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
